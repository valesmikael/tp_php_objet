<?php $user = $_SESSION['user']; ?>

<?php 

foreach ($errors as $error ){
    echo '<p class="error">' . $error . '</p>';
}
?>

<h2>Info Personnel (Nom, Prenom)</h2><br>

<ul>
<li> Nom : <?php echo $user->getNom() ?></li>
<li> Prenom: <?php echo $user->getPrenom() ?></li>
</ul>


<h2>Liste des mes biens mis a disposition avec indication si réservé + détail.</h2><br>
<?php foreach( $housings as $house ){ ?>

<div class="container">

    <?php require 'house.php' ?>


</div>

<?php } ?>

<h2>Liste des biens réservés par moi même avec date.</h2><br>


<!-- formulaire pour poster une nouvelle annonce -->

<h2>Poster une annonce.</h2>

<form enctype = "multipart / form-data" id="newhouse" action = "<?php ViewLoader::pathTo('newhouse')?>" method = "POST">
    <h1>Nouveau bien</h1>

    <label for="">
    <span>Type de logement </span><br>
        <select name="typelogement">
            <?php foreach($housings as $housing): ?>

            <option value="<?php echo $housing->getId();?>"><?php echo $housing->getTypeLogement();?></option>
            
            <?php endforeach ?>
        </select>
    </label>

    <label for="pays">
        <span>Pays</span><br>
        <input type="text" name="pays" ><br>
    </label>

    <label for="ville">
        <span>Ville</span><br>
        <input type="text" name="ville"><br>
    </label>

    <label for="surface">
        <span>Surface en m²</span><br>
        <input type="number" step="5" min="20" max="400" name="surface"><br>
    </label>

    <label for="descritif">
        <span>Description du bien</span><br>
        <textarea name="descriptif" rows="10" cols="50" ></textarea>
    </label>

    <label for="equipement">
        <span>Equipement du bien</span><br>
        <textarea  name="equipement" rows="10" cols="50" ></textarea>
    </label>

    <label for="couchage">
        <span>Nombre de couchage</span><br>
        <input type="number" min="1" max="10" name="couchage"><br>
    </label>

    <label for="photo_1">
        <span>Photo 1</span><br>
        <input type="text" id="photo_1" name="photo_1" />
    </label>


    <label for="photo_2">
        <span>Photo 2</span><br>
        <!-- <input type="hidden" name="MAX_text_SIZE" value="1024"> -->
        <input type="text" id="photo_2" name="photo_2" />
    </label>

        <input type="submit" value="Envoyer" name="register">

</form>
