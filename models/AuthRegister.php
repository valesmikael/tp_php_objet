<?php 
class AuthRegister {

    private $username;
    private $password;
    private $nom;
    private $prenom;

    public function __construct( string $username, string $password, string $nom, string $prenom ) {
        $this->username = $username;
        $this->password = hash( 'sha256', $password );
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function passwordMatch( $check_password ): bool {
        return $this->username == $check_password;
    }

    public function signin( UserRepository $userRepository ): bool{

        if( $userRepository->readByUsername( $this->username ) ) return false;

        $user = new User([
            'username' => $this->username,
            'password' => $this->password,
            'nom' => $this->nom,
            'prenom' => $this->prenom
        ]);
        
        $id = $userRepository->insert($user);

        if( $id > 0 ){

            $user->setId( $id );
            $_SESSION['user'] = $user;
            return true;
        }
        else return false;
    
    }

}