<?php 
class User extends Model {

    private $username;
    private $password;
    private $nom;
    private $prenom;
   
    public function setUsername( $username ) {
        $this->username = $username;
    }
    
    public function getUsername(): string {
        return $this->username;
    }

    public function setPassword( $password ) {
        $this->password = $password;
    }
    
    public function getPassword(): string {
        return $this->password;
    }

    public function setNom( $nom ) {
        $this->nom = $nom;
    }
    
    public function getNom(): string {
        return $this->nom;
    }

    public function setPrenom( $prenom ) {
        $this->prenom = $prenom;
    }
    
    public function getPrenom(): string {
        return $this->prenom;
    }


}