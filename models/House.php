<?php 
class House extends Model {

    private $id_housing;
    private $id_adress;
    private $id_user;
    
    private $surface;
    private $descriptif;
    private $equipement;
    private $couchage;
    private $photo_1;
    private $photo_2;
    
    public function setId_housing( $id_housing ) {
        $this->id_housing = $id_housing;
    }
    
    public function getId_housing(): int {
        return $this->id_housing;
    }
    
    public function setId_adress( $id_adress ) {
        $this->id_adress = $id_adress;
    }
    
    public function getId_adress(): int {
        return $this->id_adress;
    }
    
    public function setId_user( $id_user ) {
        $this->id_user = $id_user;
    }
    
    public function getId_user(): int {
        return $this->id_user;
    }

    public function setPays( $pays ) {
        $this->pays = $pays;
    }
    
    public function getPays(): string {
        return $this->pays;
    }
    public function setVille( $ville ) {
        $this->ville = $ville;
    }
    
    public function getVille(): string {
        return $this->ville;
    }

    public function setSurface( $surface ) {
        $this->surface = $surface;
    }
    
    public function getSurface(): int {
        return $this->surface;
    }

    public function setDescriptif( $descriptif ) {
        $this->descriptif = $descriptif;
    }
    
    public function getDescriptif(): string {
        return $this->descriptif;
    }

    public function setEquipement( $equipement ) {
        $this->equipement = $equipement;
    }
    
    public function getEquipement(): string {
        return $this->equipement;
    }
 
    public function setCouchage( $couchage ) {
        $this->couchage = $couchage;
    }
    
    public function getCouchage(): int {
        return $this->couchage;
    }
 
    public function setPhoto_1( string $photo_1 ) {
        $this->photo_1 = $photo_1;
    }
    
    public function getPhoto_1(): string {
        return $this->photo_1;
    }
 
    public function setPhoto_2( string $photo_2 ) {
        $this->photo_2 = $photo_2;
    }
    
    public function getPhoto_2(): string {
        return $this->photo_2;
    }


}