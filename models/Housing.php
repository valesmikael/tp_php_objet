<?php 
class Housing extends Model {
    
    private $typelogement;

    public function setTypeLogement( $typelogement ) {
        $this->typelogement = $typelogement;
    }
    
    public function getTypeLogement(): string {
        return $this->typelogement;
    }

}