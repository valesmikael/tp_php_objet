<?php 
class AuthLogin {

    private $username;
    private $password;

    public function __construct( string $username, string $password) {
        $this->username = $username;
        $this->password = hash( 'sha256', $password );
    }

    private function checkPassword( string $password ): bool {
        return $this->password == $password;
    }

    public function connection( UserRepository $userRepository ): bool {

        if( 
            ( $data = $userRepository->readByUsername( $this->username ) )
            && $this->checkPassword( $data['password'] )
        ) {

            $user = $userRepository->read( $data['id'] );
            $_SESSION['user'] = $user;
            return true;

        }

        return false;

    }

}