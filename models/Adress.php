<?php 
class Adress extends Model {
    
    private $pays;
    private $ville;

    public function setPays( $pays ) {
        $this->pays = $pays;
    }
    
    public function getPays(): string {
        return $this->pays;
    }

    public function setVille( $ville ) {
        $this->ville = $ville;
    }
    
    public function getVille(): string {
        return $this->ville;
    }

}