<?php 
abstract class Model {

    protected $id = 0;

    public function getId(): int {
        return $this->id;
    }

    public function setId( int $id ) {
        $this->id = $id;
    }

    public function __construct( array $data = [] ){
        $this->hydrate( $data );
    }

    private function hydrate( array $data ){

        foreach ($data as $key => $value) { //username => 'pascal'
            
            $setter = 'set' . ucfirst( $key ); // setUsername
            if( method_exists( $this, $setter ) ) { // $user ? setUsername ?

                $this->$setter( $value ); //$user->setUsername('pascal')

            }

        }

    }


}