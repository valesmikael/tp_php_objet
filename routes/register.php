<?php
$registerController = new RegisterController;

Flight::route('GET /register', [$registerController, 'info']);
Flight::route('POST /register', [$registerController, 'infopost']);
