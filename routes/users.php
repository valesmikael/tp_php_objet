<?php
$userController = new UserController;

Flight::route('GET /login', [$userController, 'login']);
Flight::route('POST /login', [$userController, 'connection']); 