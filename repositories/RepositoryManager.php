<?php 
class RepositoryManager {

    private $pdo;


    private $userRepository;
    private $houseAdressRepository;
    private $houseDetailsRepository;
    private $houseHousingRepository;

    public function getUserRepository(): UserRepository {
        return $this->userRepository;
    }

    public function getHouseAdressRepository(): HouseAdressRepository {
        return $this->houseAdressRepository;
    }

    public function getHouseHousingRepository(): HouseHousingRepository {
        return $this->houseHousingRepository;
    }

    public function getHouseDetailsRepository(): HouseDetailsRepository {
        return $this->houseDetailsRepository;
    }   
     

    public function __construct() {

        $this->pdo = (new Bdd( BDDCONFIG ))->getPdo();

        $this->houseAdressRepository = new HouseAdressRepository( $this->pdo );
        $this->houseDetailsRepository = new HouseDetailsRepository( $this->pdo );
        $this->userRepository = new UserRepository( $this->pdo );
        $this->houseHousingRepository = new HouseHousingRepository( $this->pdo );

    }

}