<?php
class UserRepository extends Repository {

    protected function getTableName(): string {
        return 'users';
    }

    public function read( int $id ): Model {
      
        $sql = "SELECT * FROM users WHERE id= :id";
        $stmt = $this->pdo->prepare( $sql );
        $stmt->execute ([
                    'id'=> $id
                    ]);

       $data = $stmt->fetch();
       return new User($data);
        
     
    }

    public function insert( Model $user ): int {

        
        $sql = "INSERT INTO users VALUES( 0, :username, :pwd, :nom, :prenom )";
        $result = $this->pdo
                ->prepare( $sql )
                ->execute([
                    'username' => $user->getUsername(),
                    'pwd'   => $user->getPassword(),
                    'nom'   => $user->getNom(),
                    'prenom' => $user->getPrenom()                  
                ]);
        
        if( !$result ) return 0;

        return $this->pdo->lastInsertId();
        

    }

    public function update( Model $user ): bool {
        

    }

    public function readAll(): array {
        

    }

    public function readByUsername( string $username ) {

        $sql = 'SELECT id, password
        FROM users
        WHERE username = :username';

        $stmt = $this->pdo->prepare( $sql );
        $stmt->execute([
            'username' => $username
        ]);

        return $stmt->fetch();

    }

}