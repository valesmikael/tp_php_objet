<?php 
abstract class Repository {

    protected $pdo;

    public function __construct( PDO $pdo ){
        $this->pdo = $pdo;
    }

    abstract protected function getTableName(): string;
    
    abstract public function read( int $id ): Model;

    abstract public function insert( Model $model ): int;

    abstract public function update( Model $model ): bool;

    abstract public function readAll(): array;
    
    public function delete( int $id ): bool {

        $sql = 'DELETE FROM ' . $this->getTableName() . ' WHERE id=:id';
        $result = $this->pdo
                    ->prepare( $sql )
                    ->execute([
                        'id' => $id
                    ]);
        
        return $result;

    }

}