<?php
class HouseDetailsRepository extends Repository {

    protected function getTableName(): string {
        return 'details';
    }

    public function read( int $id ): Model {

        $sql = 'SELECT id, surface, descriptif, equipement, couchage, photo_1, photo_2
        FROM details
        WHERE id_user=:id';

        $stmt = $this->pdo->prepare( $sql );
        $stmt->execute([
        'id' => $id_housing,
        'surface' => $surface,
        'descriptif' => $descriptif,
        'equipement' => $equipement,
        'couchage' => $couchage,
        'photo_1' => $photo_1,
        'photo_2' => $photo_2
        ]);

        // Renvoi sois false sois un tableau (assoc)
        $data = $stmt->fetchAll();

        if( !$data ) return 0;

        return new House( $data );

    }
      
    

    public function insert( Model $house_details ): int {

  
        $sql = "INSERT INTO details VALUES( 0, :surface, :descriptif, :equipement, :couchage, :photo_1, :photo_2, :id_housing, :id_adress, :id_user )";
        $stmt = $this->pdo->prepare( $sql );
        $params = [
            'surface'      => $house_details->getSurface(),
            'descriptif'   => $house_details->getDescriptif(),
            'equipement'   => $house_details->getEquipement(),
            'couchage'     => $house_details->getCouchage(),                  
            'photo_1'      => $house_details->getPhoto_1(),                  
            'photo_2'      => $house_details->getPhoto_2(),
            'id_housing' => $house_details->getId_housing(),                  
            'id_adress' => $house_details->getId_adress(),
            'id_user' => $house_details->getId_user()            
        ];
        $result = $stmt->execute($params);
                
        if( !$result ) return 0;
       
        return $this->pdo->lastInsertId();
        

    }


    public function update( Model $house_details ): bool {
        

    }

    public function readAll(): array {
        
    }

}