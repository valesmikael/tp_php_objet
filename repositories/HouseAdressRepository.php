<?php
class HouseAdressRepository extends Repository {

    protected function getTableName(): string {
        return 'adress';
    }

    public function read( int $id ): Model {


        $sql = 'SELECT adress.pays, adress.ville
        FROM details
        JOIN adress
        ON adress.id=details.id_adress
        WHERE details.id_user =:id';

        $stmt = $this->pdo->prepare( $sql );
        $stmt->execute([
        'pays' => $pays,
        'ville' => $ville,

        ]);

        // Renvoi sois false sois un tableau (assoc)
        $data = $stmt->fetchAll();

        if( !$data ) return 0;

        return new Adress( $data );
      
    }
      
    

    public function insert( Model $house_adress ): int {

  
        $sql = "INSERT INTO adress VALUES( 0, :pays, :ville)";
        $result = $this->pdo
                ->prepare( $sql )
                ->execute([
                    'pays'      => $house_adress->getPays(),
                    'ville'   => $house_adress->getVille()                
                ]);

        if( !$result ) return 0;
       
        return $this->pdo->lastInsertId();
        

    }


    public function update( Model $house_adress ): bool {
        

    }

    public function readAll(): array {
        


    }

   

}