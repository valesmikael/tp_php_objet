<?php 
require_once 'config.php';
require_once 'utils/autoloader.php';
require_once 'flight/Flight.php';
session_start();

require_once 'utils/routeloader.php';

Flight::start();