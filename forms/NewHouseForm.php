<?php

class NewHouseForm extends CheckForm {

    public function __construct() {

        parent::__construct();

        $field_type_logement = new Field ('typelogement', FieldType::TEXT, ['minsize' => 0, 'maxsize' => 20]);
        $field_pays = new Field ('pays', FieldType::TEXT, ['minsize' => 2, 'maxsize' => 20]);
        $field_ville = new Field ('ville', FieldType::TEXT, ['minsize' => 2, 'maxsize' => 20]);
        $field_surface = new Field ('surface', FieldType::INT, ['minsize' => 1, 'maxsize' => 3]);
        $field_descriptif = new Field ('descriptif', FieldType::TEXT, ['minsize' => 1, 'maxsize' => 255]);
        $field_equipement = new Field ('equipement', FieldType::TEXT, ['minsize' => 1, 'maxsize' => 255]);
        $field_couchage = new Field ('couchage', FieldType::INT, ['minsize' => 1, 'maxsize' => 2]);
        $field_photo_1 = new Field ('photo_1', '');
        $field_photo_2 = new Field ('photo_2', '');

        $this->addField($field_type_logement);
        $this->addField($field_pays);
        $this->addField($field_ville);
        $this->addField($field_surface);
        $this->addField($field_descriptif);
        $this->addField($field_equipement);
        $this->addField($field_couchage);
        $this->addField($field_photo_1);
        $this->addField($field_photo_2);


    }

}