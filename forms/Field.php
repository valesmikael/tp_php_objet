<?php
class Field {

    private $name;
    private $type;
    private $value;
    private $options;

    public function __construct( string $name, string $type, array $options =[] ){

        $this->name = $name;
        $this->type = $type;
        $this->options = $options;
        
    }public function getName(): string {
        return $this->name;
    }

    public function setValue( $value ){
        $this->value = htmlspecialchars($value);
    }

    public function getValue(){
        return $this->value;
    }

    public function getType(): string {
        return $this->type;
    }

    public function getOption( string $key ){

        if( !isset( $this->options[ $key ] ) ) return false;
        return $this->options[ $key ];

    }


}