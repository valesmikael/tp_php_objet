<?php

class LoginForm extends CheckForm {

    public function __construct() {

        parent::__construct();

        $field_user = new Field ('username', FieldType::TEXT, ['minsize' => 5, 'maxsize' => 12]);
        $field_pass = new Field ('password', FieldType::TEXT, ['minsize' => 5, 'maxsize' => 12]);

        $this->addField($field_user);
        $this->addField($field_pass);

    }

}