<?php

class RegisterForm extends CheckForm {

    public function __construct() {

        parent::__construct();

        $field_user = new Field ('username', FieldType::TEXT, ['minsize' => 5, 'maxsize' => 12]);
        $field_pass = new Field ('pwd', FieldType::TEXT, ['minsize' => 5, 'maxsize' => 12]);
        $field_pass_check = new Field ('pwd_check', FieldType::TEXT, ['minsize' => 5, 'maxsize' => 12]);
        $field_nom = new Field ('nom', FieldType::TEXT, ['minsize' => 5, 'maxsize' => 12]);
        $field_prenom = new Field ('prenom', FieldType::TEXT, ['minsize' => 5, 'maxsize' => 12]);

        $this->addField($field_user);
        $this->addField($field_pass);
        $this->addField($field_pass_check);
        $this->addField($field_nom);
        $this->addField($field_prenom);

    }

}