<?php
abstract class CheckForm {

    protected $fields = [];
    protected $datas = [];
    protected $errors = [];

    public function __construct(){
        $this->datas = $_POST;
    }

    public function getErrors(): array {
        return $this->errors;
    }

    public function getField( string $name): Field {

       foreach( $this->fields as $field){

            if( $field->getname() == $name ) {
                return $field;
            }

       }

    }

    protected function addfield ( $field) {
        $this->fields[] = $field;
    }

    protected function checkexists(){
              //$_POST
        foreach( $this->fields as $field){

            $name = $field->getName();
            // isset $_POST[ 'username' ]

            if( !empty( $this->datas[ $name ] ) ){

                $field->setValue( $this->datas[ $name ] );

            }
            else{
                $this->errors[] = 'Le champs ' . $name . ' n\'existe pas !'; 
            }


        }  
    }

    protected function checkTypes(){

        foreach ($this->fields as $field){

            if (!$field->getValue() ) continue;
            $success = false;

            switch ( $field->getType() ){

                case FieldType::TEXT :
                    $success = filter_var(
                        $field->getValue(),
                        FILTER_VALIDATE_REGEXP,
                        ['options' => ['regexp' => '/^[^#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]+$/']]
                        );
                break;

                case FieldType::INT :
                    $success = filter_var(
                        $field->getValue(),
                        FILTER_VALIDATE_INT
                    );
                break;

                case FieldType::FLOAT :
                $success = filter_var(
                    $field->getValue(),
                    FILTER_VALIDATE_FLOAT
                );
                break;

                case FieldType::REGEXP :
                $success = filter_var(
                    $field->getValue(),
                    FILTER_VALIDATE_REGEXP,
                    ['options' => ['regexp' => $field->getOption('regexp')]]
                );
                break;

                default: 
                    $success = true;
                    break;

            }

            if ( !$success ) {
                $this->errors[] = 'Le champs ' . $field->getName() . ' est incorrect.';
            }
            

        }

    }

    protected function checkOptions(){

        foreach ( $this->fields as $field ){

            if ( !$field->getValue() ) continue;

            $minsize = $field->getOption('minsize');
            $maxsize = $field->getOption('maxsize');
            $name = $field->getName();
            $value = $field->getValue();

            if( $minsize && ( strlen( $value ) < $minsize )){

                    $this->errors[] = 'LA valeur du champs '
                    . $name
                    . ' est trop courte ('
                    . $minsize
                    . ' caracteres minimum)';
            } 
            
            if( $maxsize && ( strlen( $value ) > $maxsize )){

                    $this->errors[] = 'LA valeur du champs '
                    . $name
                    . ' est trop longue ('
                    . $maxsize
                    . ' caracteres maximum)';
            }

        }

    }


    public function check(): bool {

        $this->checkExists();
        $this->checkTypes();
        $this->checkOptions();

        return empty( $this->errors );

    }

}