<?php 
function airbnb_autoload( $class_name ) {

    $folders = [
        'controllers',
        'models',
        'utils',
        'repositories',
        'forms'
    ];

    foreach ( $folders as $folder ) {

        $file = $folder . '/' . $class_name . '.php';
        
        if( file_exists( $file ) ){
            require_once $file;
            return;
        }

    }


}
spl_autoload_register('airbnb_autoload');