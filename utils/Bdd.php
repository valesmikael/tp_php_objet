<?php 
class Bdd {

    private $driver;
    private $database;
    private $host;
    private $user;
    private $password;
    private $pdo;
    private $specialconfig;

    public function __construct( array $config ) {

        $this->driver   = $config['DRIVER'];
        $this->database = $config['DATABASE'];
        $this->host     = $config['HOST'];
        $this->user     = $config['USER'];
        $this->password = $config['PASSWORD'];
        $this->specialconfig = [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];

        $this->setPdo();

    }

    private function setPdo() {

        
        $dsn = $this->driver . ':dbname=' . $this->database . ';host=' .$this->host; 

        $this->pdo = new PDO(
            $dsn,
            $this->user,
            $this->password,
            $this->specialconfig
        );

    }

    public function getPdo(): PDO {
        return $this->pdo;
    }

}