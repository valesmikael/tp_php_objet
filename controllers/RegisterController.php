<?php
class RegisterController extends Controller {


    
    public function info(){
    
            $errors=[];
            if (isset($_SESSION['form_errors']) ){
                $errors = $_SESSION['form_errors'];
                unset ( $_SESSION['form_errors']);  
            }    
    
            $this->loadView('users/register', [
            'title'  => 'register',
            'errors' => $errors
        ]);    
    
    }
    
    public function infopost() {
            
        /**
         * Verification formulaire
         */
        $registerForm = new RegisterForm();
    
        if ( !$registerForm->check() ){
            $_SESSION['form_errors'] = $registerForm->getErrors();
            $this->redirect('/register');
        }

        /**
         * Recuperer les valeurs du formulaire
         */
        $username = $registerForm->getField('username')->getValue();
        $password = $registerForm->getField('pwd')->getValue();
        $password_check = $registerForm->getField('pwd_check')->getValue();
        $nom = $registerForm->getField('nom')->getValue();
        $prenom = $registerForm->getField('prenom')->getValue();

        /**
         * Commence l'Authentification / Enregistrement
         */
        $auth = new AuthRegister( $username, $password, $nom, $prenom );
        $userRepository = $this->repo_manager->getUserRepository();

        if( $auth->passwordMatch( $password_check ) && $auth->signin( $userRepository ) ) {
            
            echo '<p> ok </p>';

        }
        else {
            echo "Pas ok";
        }
    
    }

       
    
    }
    

