<?php
abstract class CheckController extends Controller {


    protected function checkAccess() {

        if( !$this->canAccess() ){
            $this->errorAction();
        }

    }

    abstract protected function canAccess(): bool;
    abstract protected function errorAction();

}
