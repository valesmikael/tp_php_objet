<?php

class NewHouseController extends Controller {
    
    public function info(){
    
            $errors=[];
            if (isset($_SESSION['form_errors']) ){
                $errors = $_SESSION['form_errors'];
                unset ( $_SESSION['form_errors']);  
            }    
    
            $this->loadView('users/myspace', [
            'title'  => 'myspace',
            'errors' => $errors
        ]);    
    
    }
    
    public function infopost() {
            
        /**
         * Verification formulaire
         */
        $newhouseform = new NewHouseForm();
    
        if ( !$newhouseform->check() ){
            $_SESSION['form_errors'] = $newhouseform->getErrors();
            $this->redirect('/myspace');
        }

        /**
         * Recuperer les valeurs du formulaire
         */
        $type_logement = $newhouseform->getField('typelogement')->getValue();
        $pays = $newhouseform->getField('pays')->getValue();
        $ville = $newhouseform->getField('ville')->getValue();
        $surface = $newhouseform->getField('surface')->getValue();
        $descriptif = $newhouseform->getField('descriptif')->getValue();
        $equipement = $newhouseform->getField('equipement')->getValue();
        $couchage = $newhouseform->getField('couchage')->getValue();
        $photo_1 = $newhouseform->getField('photo_1')->getValue();
        $photo_2 = $newhouseform->getField('photo_2')->getValue();
        
        
        
        // var_dump( $_POST ); die;
        //var_dump( $_FILES ); die;

        /**
         * Commence l'Authentification / Enregistrement
         */
       
        $house_adress = new Adress([
            "pays" => $pays, 
            "ville" => $ville 
        ]);
        $houseAdressRepository = $this->repo_manager->getHouseAdressRepository();
        $id_adress = $houseAdressRepository->insert($house_adress);
        $house_adress->setId( $id_adress );

        if( $id_adress == 0 ) {
            echo "enregistrement non effectué (Echec adresse)";
        }
        
        $house_details = new House( [
            "id_housing" => $type_logement,
            "id_adress"  => $id_adress, 
            "surface"    => $surface, 
            "descriptif" => $descriptif, 
            "equipement" => $equipement, 
            "couchage"   => $couchage, 
            "photo_1"    => $photo_1, 
            "photo_2"    => $photo_2,
            "id_user"   => $_SESSION['user']->getId()
        ] );
        $houseDetailsRepository = $this->repo_manager->getHouseDetailsRepository();
        $id_details = $houseDetailsRepository->insert( $house_details );
        $house_details->setId( $id_details );


        if( $id_details > 0 ) {
            $this->redirect('/myspace');
        }
        else {
            echo "enregistrement non effectué";
        }
    
    }


}