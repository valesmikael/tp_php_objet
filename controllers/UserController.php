<?php
class UserController extends Controller {

    public function login() {

        $errors=[];
        if (isset($_SESSION['form_errors']) ){
            $errors = $_SESSION['form_errors'];
            unset ( $_SESSION['form_errors']);  
        }
        

        $this->loadView('users/login', [
            'title'  => 'Login',
            'errors' => $errors
        ]);
        
    }

    public function connection() {
        
        $loginForm =new loginForm();

        if ( !$loginForm->check() ){
            $_SESSION['form_errors'] = $loginForm->getErrors();
            $this->redirect('/login');
        }

        $username = $loginForm->getField('username')->getValue();
        $password = $loginForm->getField('password')->getValue();
      
        $auth = new AuthLogin( $username, $password );

        if( $auth->connection( $this->repo_manager->getUserRepository() ) ){
            $this->redirect('/myspace');
        }
        else{
            $this->redirect('/login', 'LOGIN');
        }

    }

}