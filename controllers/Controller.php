<?php 
abstract class Controller {

    protected $user;    
    protected $repo_manager;

    public function __construct(){

        $this->repo_manager = new RepositoryManager();

        if( isset( $_SESSION['user'] ) ){
            $this->user = $_SESSION['user'];
            
        }

    }

    protected function isLogged(): bool {

        // Type casting -> prend la valeur booleene de la variable
        // Exemple: $a = (bool) 112; devient $a = true;
        return (bool) $this->user;
        
    }

    protected function redirect( $direction, $error = false ) {

        if( $error ) $direction .= '?error=' . $error;

        Flight::redirect( $direction ); die;

    }

    protected function loadView( $view, array $vars = [] ) {

        extract( $vars );

        require_once 'views/partials/header.php';
        require_once 'views/partials/nav.php';
        require_once 'views/' . $view . '.php';
        require_once 'views/partials/footer.php';
        
    }

}