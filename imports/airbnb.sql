-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour airbnb
DROP DATABASE IF EXISTS `airbnb`;
CREATE DATABASE IF NOT EXISTS `airbnb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `airbnb`;

-- Export de la structure de la table airbnb. adress
DROP TABLE IF EXISTS `adress`;
CREATE TABLE IF NOT EXISTS `adress` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pays` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table airbnb. details
DROP TABLE IF EXISTS `details`;
CREATE TABLE IF NOT EXISTS `details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `surface` int(11) NOT NULL,
  `descriptif` varchar(255) NOT NULL,
  `equipement` varchar(255) NOT NULL,
  `couchage` int(11) NOT NULL,
  `photo_1` varchar(50) NOT NULL,
  `photo_2` varchar(50) NOT NULL,
  `id_housing` int(10) unsigned NOT NULL,
  `id_adress` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1_details_housing` (`id_housing`),
  KEY `FK2_details_adress` (`id_adress`),
  KEY `FK3_details_users` (`id_user`),
  CONSTRAINT `FK1_details_housing` FOREIGN KEY (`id_housing`) REFERENCES `housing` (`id`),
  CONSTRAINT `FK2_details_adress` FOREIGN KEY (`id_adress`) REFERENCES `adress` (`id`),
  CONSTRAINT `FK3_details_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table airbnb. housing
DROP TABLE IF EXISTS `housing`;
CREATE TABLE IF NOT EXISTS `housing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typelogement` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table airbnb. reserv
DROP TABLE IF EXISTS `reserv`;
CREATE TABLE IF NOT EXISTS `reserv` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `id_details` int(10) unsigned NOT NULL,
  `id_users` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1_reserv_details` (`id_details`),
  KEY `FK2_reserv_users` (`id_users`),
  CONSTRAINT `FK1_reserv_details` FOREIGN KEY (`id_details`) REFERENCES `details` (`id`),
  CONSTRAINT `FK2_reserv_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table airbnb. users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(70) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
